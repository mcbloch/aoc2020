# AoC2020

My submissions for the [Advent of Code](https://adventofcode.com) 2020.

## Creating a new daily entry

- Login to the website and download your session cookie.
- Save this cookie in your `.env` file in the `AoCCookie` variable.
- Run the init script.
  It downloads the new input file and makes to files to start coding in.

```sh
    ./init_day.sh 01
```

## Running

Running all solutions. This will print them in a table.

TBD

## Tests

```sh
mix test
```

Watch files and auto-execute tests

```sh
mix test.watch
```
