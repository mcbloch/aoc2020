#!/bin/sh

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 NUMBER" >&2
  exit 1
fi

DAY=$(($1 + 0))

COOKIE=$AoCCookie

curl \
  -v \
  -fSL -o "priv/input_${DAY}.txt" \
  -H "cookie:$COOKIE" \
  "https://adventofcode.com/2020/day/$1/input" \
