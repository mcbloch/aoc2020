#!/bin/bash

if (($# != 1)); then
  echo "Illegal number of parameters"
  echo "Please provide a day number like this: \"$0 01\"" && exit
fi

source .env
DAY=$1
MOD="Day${DAY}"
#./get-input.sh "$DAY" || echo "Could not fetch input" && exit 1

SRC=$(
  cat <<EOF
defmodule ${MOD} do
  @moduledoc """
  Documentation for \`${MOD}\`.
  """

  @doc """
  Ex1:

  ## Examples

      iex> ${MOD}.ex1()
      "hello world"

  """
  def ex1 do
    Shared.read_input_of_day("${DAY}")
  end

  @doc """
  Ex2:

  ## Examples

      iex> ${MOD}.ex2()
      "hello world"

  """
  def ex2 do
    Shared.read_input_of_day("${DAY}")
  end
end
EOF
)

TEST=$(
  cat <<EOF
defmodule ${MOD}Test do
  use ExUnit.Case
  doctest ${MOD}
end

EOF
)

SRC_FILE="lib/day${DAY}.ex"
TEST_FILE="test/day${DAY}_test.exs"

if [ -f "$SRC_FILE" ]; then
  echo "$SRC_FILE exists."
else
  echo "Generated source file at \"${SRC_FILE}\""
  echo "$SRC" >"$SRC_FILE"
fi

if [ -f "$TEST_FILE" ]; then
  echo "$TEST_FILE exists."
else
  echo "Generated test file at \"${TEST_FILE}\""
  echo "$TEST" >"$TEST_FILE"
fi
