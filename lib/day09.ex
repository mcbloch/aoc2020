defmodule Day09 do

  def parse_input(filename) do
    filename
    |> Utils.slorp
    |> Enum.map(&String.trim/1)
    |> Enum.map(&String.to_integer/1)
  end

  def check_valid(chunk) do
    Enum.slice(chunk, 0..-2)
    |> Utils.combinations(2)
    |> Enum.find(fn comb -> Enum.sum(comb) == List.last(chunk) end)
  end

  @doc """
          iex> Day09.ex1(Utils.filepath_of_day("09_example"), 5)
          127

          iex> Day09.ex1()
          10884537
  """
  def ex1, do: ex1(Utils.filepath_of_day("09"), 25)
  def ex1(filename), do: ex1(filename, 25)
  def ex1(filename, preamble_size) do
    IO.puts("Running 9.1")
    filename
    |> parse_input
    |> Stream.chunk_every(preamble_size + 1, 1)
    |> Enum.find(fn chunk -> !check_valid(chunk) end)
    |> List.last
  end

  @doc """
        iex> Day09.ex2(Utils.filepath_of_day("09_example"), 5)
        62

        iex> Day09.ex2()
        1261309
  """
  def ex2, do: ex2(Utils.filepath_of_day("09"))
  def ex2(filename), do: ex2(filename, 25)
  def ex2(filename, preamble_size) do
    IO.puts("Running 9.2")
    data = parse_input(filename)
    {error_value, error_index} =
      data
      |> Stream.chunk_every(preamble_size + 1, 1)
      |> Enum.with_index(preamble_size)
      |> Enum.find(fn chunk -> !check_valid(elem(chunk, 0)) end)

    error_value = List.last(error_value)

    Enum.find_value(
      0..error_index - 2,
      fn start_i ->
        Enum.find_value(
          start_i + 1..error_index - 1,
          fn end_i ->
            range = Enum.slice(data, start_i..end_i)
            if Enum.sum(range) == error_value do
              Enum.max(range) + Enum.min(range)
            end
          end
        )
      end
    )
  end
end
