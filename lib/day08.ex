defmodule Day08 do

  defmodule Parallel do
    @doc """
        iex> Parallel.map([1,2,3], &(&1*2))
        [2,4,6]
    """
    def map(collection, func) do
      collection
      |> Enum.map(&(Task.async(fn -> func.(&1) end)))
      |> Enum.map(&Task.await/1)
    end
  end

  def parse_program(filename) do
    filename
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&String.split/1)
    |> Enum.map(fn [code, arg] -> [code, String.to_integer(arg)] end)
  end

  def execute(program), do: execute(program, 0, 0, MapSet.new)
  def execute(program, position, acc, history) do
    if MapSet.member?(history, position) do
      {:error, acc}
    else
      history = MapSet.put(history, position)
      case Enum.at(program, position) do
        ["nop", _] -> execute(program, position + 1, acc, history)
        ["jmp", arg] -> execute(program, position + arg, acc, history)
        ["acc", arg] -> execute(program, position + 1, acc + arg, history)
        nil -> {:ok, acc}
      end
    end
  end

  @doc """
        iex> Day08.ex1(Utils.filepath_of_day("08_example"))
        5

        iex> Day08.ex1()
        1671
  """
  def ex1, do: ex1(Utils.filepath_of_day("08"))
  def ex1(filename) do
    IO.puts("Running 8.1")
    filename
    |> parse_program
    |> execute
    |> elem(1)
  end

  @doc """
      iex> Day08.ex2(Utils.filepath_of_day("08_example"))
      8

      iex> Day08.ex2()
      892
  """
  def ex2, do: ex2(Utils.filepath_of_day("08"))
  def ex2(filename) do
    IO.puts("Running 8.2")
    p = parse_program(filename)

    Enum.with_index(p)
    |> Enum.filter(fn {[code, _], _} -> code in ["nop", "jmp"] end)
    |> Parallel.map(
         fn {[code, arg], i} ->
           new_code = case code do
             "nop" -> "jmp"
             "jmp" -> "nop"
           end
           List.replace_at(p, i, [new_code, arg])
           |> execute
         end
       )
    |> Keyword.pop!(:ok)
    |> elem(0)
  end
end
