defmodule Day03 do
  @moduledoc """
  Documentation for `Day03`.
  """

  def count_trees(input, [step_right, step_down]) do
    width = String.length(Enum.at(input, 0))
    input
    |> Enum.with_index
    |> Enum.count(
         fn ({v, row_i}) ->
           if rem(row_i, step_down) == 0 do
             String.at(v, rem(div(row_i, step_down) * step_right, width)) == "#"
           end
         end
       )
  end

  @doc """
      iex> Day03.ex1()
      257
  """
  def ex1, do: ex1(Utils.filepath_of_day("03"))
  def ex1(file) do
    IO.puts("Running 3.1")
    file
    |> Utils.slorp
    |> count_trees([3, 1])
  end

  @doc """
      iex> Day03.ex2() > 1320379648
      true

      iex> Day03.ex2()
      1744787392

      iex> Day03.count_trees(Utils.slorp(Utils.filepath_of_day("03_example")), [1,1])
      2

      iex> Day03.count_trees(Utils.slorp(Utils.filepath_of_day("03_example")), [3,1])
      7

      iex> Day03.count_trees(Utils.slorp(Utils.filepath_of_day("03_example")), [5,1])
      3

      iex> Day03.count_trees(Utils.slorp(Utils.filepath_of_day("03_example")), [7,1])
      4

      iex> Day03.count_trees(Utils.slorp(Utils.filepath_of_day("03_example")), [1,2])
      2

      iex> Day03.ex2(Utils.filepath_of_day("03_example"))
      336
  """
  def ex2, do: ex2(Utils.filepath_of_day("03"))
  def ex2(file) do
    IO.puts("Running 3.2")
    input = Utils.slorp(file)

    [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]
    |> Enum.map(&count_trees(input, &1))
    |> Enum.reduce(1, &(&1 * &2))
  end
end
