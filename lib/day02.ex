defmodule Day02 do

  def extract_groups(row) do
    String.split(row, ["-", " ", ":"])
    |> (fn k ->
      %{
        :first => Enum.at(k, 0),
        :second => Enum.at(k, 1),
        :char => Enum.at(k, 2),
        :pass => Enum.at(k, 4)
      }
        end).()
    |> Map.update!(:first, &String.to_integer/1)
    |> Map.update!(:second, &String.to_integer/1)
  end

  def is_min_max(%{:first => min, :second => max, :char => char, :pass => pass}) do
    pass
    |> String.graphemes
    |> Enum.count(& &1 == char)
    |> (&
          &1 in min..max).()
  end

  def is_char_at(%{:first => first, :second => second, :char => c, :pass => pass}) do
    first_c = String.at(pass, first - 1)
    second_c = String.at(pass, second - 1)
    (first_c == c and second_c != c) or (second_c == c and first_c != c)
  end

  @doc """
      iex> Day02.ex1() > 274
      true

      iex> Day02.ex1()
      572
  """
  def ex1, do: ex1(Utils.filepath_of_day("02"))
  def ex1(input_file) do
    IO.puts("Running 2.1")
    input_file
    |> Utils.slorp
    |> Enum.map(&extract_groups/1)
    |> Enum.filter(&is_min_max/1)
    |> Enum.count
  end

  @doc """
      iex> Day02.ex2() < 454
      true

      iex> Day02.ex2()
      306
  """
  def ex2, do: ex2(Utils.filepath_of_day("02"))
  def ex2(input_file) do
    IO.puts("Running 2.2")
    input_file
    |> Utils.slorp
    |> Enum.map(&extract_groups/1)
    |> Enum.filter(&is_char_at/1)
    |> Enum.count
  end
end
