defmodule Day05 do
  def pow_2(x), do: Bitwise.bsl(1, x)

  def get_bit_value(grapheme) do
    case grapheme do
      "R" -> 1
      "B" -> 1
      "L" -> 0
      "F" -> 0
    end
  end


  @doc """
    iex> Day05.calculate_seat_id("FBFBBFFRLR")
    357

    iex> Day05.calculate_seat_id("BFFFBBFRRR")
    567

    iex> Day05.calculate_seat_id("FFFBBBFRRR")
    119

    iex> Day05.calculate_seat_id("BBFFBBFRLL")
    820
  """
  def calculate_seat_id(v), do: calculate_seat_id_with_reducer(v)
  def calculate_seat_id_with_bits(v) do
    <<n :: 10>> = for code <- String.codepoints(v), do: <<get_bit_value(code) :: 1>>, into: <<>>
    n
  end

  def calculate_seat_id_with_reducer(v) do
    String.codepoints(v)
    |> Enum.with_index(1)
    |> Enum.map(
         fn ({val, i}) ->
           case val do
             "R" -> div(1024, pow_2(i))
             "B" -> div(1024, pow_2(i))
             "L" -> 0
             "F" -> 0
           end
         end
       )
    |> Enum.sum
  end

  @doc """
    iex> Day05.ex1()
    928
  """
  def ex1, do: ex1(Utils.filepath_of_day("05"))
  def ex1(input_file) do
    IO.puts("Running 5.1")
    input_file
    |> Utils.slorp
    |> Enum.map(&calculate_seat_id_with_bits/1)
    |> Enum.max
  end

  @doc """
    iex> Day05.ex2()
    610
  """
  def ex2, do: ex2(Utils.filepath_of_day("05"))
  def ex2(input_file) do
    IO.puts("Running 5.2")
    input_file
    |> Utils.slorp
    |> Enum.map(&calculate_seat_id_with_bits/1)
    |> Enum.sort
    |> (&Enum.zip(&1, tl(&1))).()
    |> Enum.find(fn ({f, s}) -> s != f + 1 end)
    |> elem(0)
    |> Kernel.+(1)
  end
end
