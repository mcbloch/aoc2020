defmodule Day06 do

  def read(input_file) do
    input_file
    |> File.read!
    |> String.trim
    |> String.split("\n\n")
    |> Enum.map(&String.split/1)
  end

  def count_unique_in_group(group) do
    group
    |> Enum.map(&String.codepoints/1)
    |> List.flatten
    |> MapSet.new
    |> MapSet.size
  end

  def count_intersection_in_group(group) do
    group
    |> Enum.map(&String.codepoints/1)
    |> Enum.map(&MapSet.new/1)
    |> Enum.reduce(&MapSet.intersection/2)
    |> MapSet.size
  end

  @doc """
      iex> Day06.ex1(Utils.filepath_of_day("06_example"))
      11

      iex> Day06.ex1()
      7027
  """
  def ex1, do: ex1(Utils.filepath_of_day("06"))
  def ex1(input_file) do
    IO.puts("Running 6.1")
    input_file
    |> read
    |> Enum.map(&count_unique_in_group/1)
    |> Enum.sum
  end

  @doc """
    iex> Day06.ex2(Utils.filepath_of_day("06_example"))
    6

    iex> Day06.ex2()
    3579
  """
  def ex2, do: ex2(Utils.filepath_of_day("06"))
  def ex2(input_file) do
    IO.puts("Running 6.2")
    input_file
    |> read
    |> Enum.map(&count_intersection_in_group/1)
    |> Enum.sum
  end
end
