defmodule Day01 do

  def solve(list, n) do
    Enum.find_value(
      Utils.combinations(list, n),
      fn item ->
        if Enum.sum(item) === 2020 do
          Enum.reduce(item, 1, &(&1 * &2))
        end
      end
    )
  end

  def read(input_file) do
    input_file
    |> Utils.slorp
    |> Enum.map(&String.to_integer/1)
  end

  @doc """
      iex> Day01.solve([1721,979,366,299,675,1456], 2)
      514579

      iex> Day01.ex1()
      158916
  """
  def ex1(), do: ex1(Utils.filepath_of_day("01"))
  def ex1(input_file) do
    IO.puts("Running 1.1")
    read(input_file)
    |> solve(2)
  end

  @doc """
      iex> Day01.solve([1721,979,366,299,675,1456], 3)
      241861950

      iex> Day01.ex2()
      165795564
  """
  def ex2(), do: ex2(Utils.filepath_of_day("01"))
  def ex2(input_file) do
    IO.puts("Running 1.2")
    read(input_file)
    |> solve(3)
  end
end
