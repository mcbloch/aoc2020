defmodule Mix.Tasks.RunDay do
  use Mix.Task

  @shortdoc "Run a specific specific day."

  def run(args) do
    day = Enum.at(args, 0)
    part = Enum.at(args, 1)
    file = Enum.at(args, 2)

    IO.puts "Running day #{day}, part #{part} with input #{file}"

    solutions = %{
      "01" => %{
        "1" => &Day01.ex1/1,
        "2" => &Day01.ex2/1
      },
      "02" => %{
        "1" => &Day02.ex1/1,
        "2" => &Day02.ex2/1
      },
      "03" => %{
        "1" => &Day03.ex1/1,
        "2" => &Day03.ex2/1
      },
      "04" => %{
        "1" => &Day04.ex1/1,
        "2" => &Day04.ex2/1
      },
      "05" => %{
        "1" => &Day05.ex1/1,
        "2" => &Day05.ex2/1
      },
      "06" => %{
        "1" => &Day06.ex1/1,
        "2" => &Day06.ex2/1
      },
      "07" => %{
        "1" => &Day07.ex1/1,
        "2" => &Day07.ex2/1
      },
      "08" => %{
        "1" => &Day08.ex1/1,
        "2" => &Day08.ex2/1
      },
      "09" => %{
        "1" => &Day09.ex1/1,
        "2" => &Day09.ex2/1
      },
    }

    solutions
    |> Map.get(day)
    |> Map.get(part)
    |> (fn (f) ->
      {uSecs, :ok} =
        :timer.tc(
          fn ->
            f.(file)
            :ok
          end
        )
      IO.puts "#{""} | Took #{uSecs} usecs   \t (#{uSecs / 1_000_000} s)"
        end).()
    |> IO.puts
  end
end

