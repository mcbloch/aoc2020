defmodule Day04 do
  @doc """
  iex> Day04.valid_field?(:byr, "2002")
  true

  iex> Day04.valid_field?(:byr, "2003")
  false


  iex> Day04.valid_field?(:hgt, "60in")
  true

  iex> Day04.valid_field?(:hgt, "190cm")
  true

  iex> Day04.valid_field?(:hgt, "190in")
  false

  iex> Day04.valid_field?(:hgt, "190")
  false


  iex> Day04.valid_field?(:hcl, "#123abc")
  true

  iex> Day04.valid_field?(:hcl, "#123abz")
  false

  iex> Day04.valid_field?(:hcl, "123abc")
  false


  iex> Day04.valid_field?(:ecl, "brn")
  true

  iex> Day04.valid_field?(:ecl, "wat")
  false

  iex> Day04.valid_field?(:pid, "000000001")
  true

  iex> Day04.valid_field?(:pid, "0123456789")
  false
  """
  def valid_field?(:byr, v), do: String.to_integer(v) in 1920..2002
  def valid_field?(:iyr, v), do: String.to_integer(v) in 2010..2020
  def valid_field?(:eyr, v), do: String.to_integer(v) in 2020..2030
  def valid_field?(:hgt, <<v :: binary - size(3)>> <> "cm"), do: String.to_integer(v) in 150..193
  def valid_field?(:hgt, <<v :: binary - size(2)>> <> "in"), do: String.to_integer(v) in 59..76
  def valid_field?(:hgt, _), do: false
  def valid_field?(:hcl, v), do: String.match?(v, ~r/^#[0-9a-f]{6}$/)
  def valid_field?(:ecl, v), do: v in ~w(amb blu brn gry grn hzl oth)
  def valid_field?(:pid, v), do: String.match?(v, ~r/^[0-9]{9}$/)


  @doc """
  The main solution.

  Parses the input in blocks that represent 1 passport.
  Then splits those in the different fields on the passport.
  Lastly loops over the different validators, checks if the required fields are present
  and if validate_content? is true also applies the validator on the content.
  """
  def count_valid_passwords(input_file, validate_content?) do
    validators = ~w(byr iyr eyr hgt hcl ecl pid)a

    split_pattern = :binary.compile_pattern([" ", "\n", ":"])
    input_file
    |> File.read!
    |> String.trim
    |> String.split("\n\n")
    |> Enum.count(
         (fn pass_raw ->
           passport =
             String.split(pass_raw, split_pattern)
             |> Enum.chunk_every(2)
             |> Map.new(fn [k, v] -> {String.to_atom(k), v} end)

           Enum.all?(
             validators,
             fn (k) ->
               Map.has_key?(passport, k) and
               (!validate_content? or valid_field?(k, Map.get(passport, k)))
             end
           )
          end)
       )
  end

  @doc """
  iex> Day04.ex1()
  260

  iex> Day04.ex1(Utils.filepath_of_day("04_example"))
  2
  """
  def ex1, do: ex1(Utils.filepath_of_day("04"))
  def ex1(input_file) do
    IO.puts("Running 4.1")
    count_valid_passwords(input_file, false)
  end

  @doc """
  iex> Day04.ex2()
  153

  iex> Day04.ex2(Utils.filepath_of_day("04_invalid"))
  0

  iex> Day04.ex2(Utils.filepath_of_day("04_valid"))
  4
  """
  def ex2, do: ex2(Utils.filepath_of_day("04"))
  def ex2(input_file) do
    IO.puts("Running 4.2")
    count_valid_passwords(input_file, true)
  end
end
