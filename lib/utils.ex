defmodule Utils do
  @moduledoc false

  def filepath_of_day(day) do
    Path.join(:code.priv_dir(:aoc2020), "input_#{day}.txt")
  end

  def slorp(input_file) do
    input_file
    |> File.read!
    |> String.trim
    |> String.split("\n")
  end

  @doc """
  This function lists all combinations of `num` elements from the given `list`
  """
  def combinations(list, num)
  def combinations(_list, 0), do: [[]]
  def combinations(list = [], _num), do: list
  def combinations([head | tail], num) do
    Enum.map(combinations(tail, num - 1), &[head | &1]) ++
    combinations(tail, num)
  end
end
