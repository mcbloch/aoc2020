defmodule Day07 do

  def parse_line_to_reverse_edge_pairs(line) do
    [from, tos_raw] = String.split(line, " bags contain ")

    tos = tos_raw
          |> String.split(", ")
          |> Enum.map(
               fn to_raw ->
                 to_raw
                 |> String.split(" ")
                 |> Enum.slice(1..-2)
                 |> Enum.join(" ")
               end
             )

    for to <- tos, do: {to, from}, into: []
  end

  def parse_line_to_edges(line) do
    [from, tos_raw] =
      line
      |> String.split(" bags contain ")

    tos = tos_raw
          |> String.split(", ")
          |> Enum.map(
               (fn to_raw ->
                 parts = String.split(to_raw, " ")
                 %{
                   :weight => parts
                              |> hd
                              |> String.to_integer,
                   :to => parts
                          |> Enum.slice(1..-2)
                          |> Enum.join(" ")
                 }
                end)
             )

    %{from => tos}
  end



  def merge_edges_into_graph(edges) do
    Enum.reduce(
      edges,
      fn x, y ->
        Map.merge(x, y, fn _k, v1, v2 -> v2 ++ v1 end)
      end
    )
  end


  def flat_group(pairs) do
    Enum.reduce(
      pairs,
      %{},
      fn ({to, from}, grouped) ->
        Map.update(grouped, to, MapSet.new([from]), &MapSet.put(&1, from))
      end
    )
  end


  def recursive_count_incoming(reverse_edges, to, history) do
    history = MapSet.put(history, to)
    incoming_edges = Map.get(reverse_edges, to)

    case incoming_edges do
      nil -> history
      _ -> Enum.reduce(
             incoming_edges,
             history,
             fn (from, local_hist) ->
               if !MapSet.member?(local_hist, from) do
                 recursive_count_incoming(reverse_edges, from, local_hist)
               else
                 local_hist
               end
             end
           )
    end
  end
  def recursive_count_incoming(reverse_edges, to), do: recursive_count_incoming(reverse_edges, to, MapSet.new())

  @doc """
          iex> Day07.ex1(Utils.filepath_of_day("07_example"))
          4

          iex> Day07.ex1()
          348
  """
  def ex1, do: ex1(Utils.filepath_of_day("07"))
  def ex1(input_file) do
    IO.puts("Running 7.1")
    input_file
    |> Utils.slorp
    |> Enum.filter(&!String.contains?(&1, "no other bags."))
    |> Enum.map(&parse_line_to_reverse_edge_pairs/1)
    |> List.flatten
    |> flat_group
    |> recursive_count_incoming("shiny gold")
    |> Enum.count
    |> Kernel.-(1)
  end

  def find_polytree_from_node(graph, from) do
    case Map.has_key?(graph, from) do
      false ->
        []
      true ->
        for edge <- Map.get(graph, from),
            into: [], do: %{node: edge, children: find_polytree_from_node(graph, Map.get(edge, :to))}
    end
  end

  def recursive_children_weight_sums(tree) do
    Enum.reduce(
      tree,
      0,
      fn (branch, sum) ->
        sum +
          Map.get(Map.get(branch, :node), :weight) +
          (Map.get(Map.get(branch, :node), :weight) *
             recursive_children_weight_sums(Map.get(branch, :children)))
      end
    )
  end

  @doc """
        iex> Day07.ex2(Utils.filepath_of_day("07_example"))
        32

        iex> Day07.ex2(Utils.filepath_of_day("07_example_2"))
        126

        iex> Day07.ex2()
        18885
  """
  def ex2, do: ex2(Utils.filepath_of_day("07"))
  def ex2(input_file) do
    IO.puts("Running 7.2")
    input_file
    |> Utils.slorp
    |> Enum.filter(&!String.contains?(&1, "no other bags."))
    |> Enum.map(&parse_line_to_edges/1)
    |> merge_edges_into_graph
    |> find_polytree_from_node("shiny gold")
    |> recursive_children_weight_sums
  end
end
